﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFSM : MonoBehaviour
{
    private EnemyStateBase currentState;

    public EnemyStateIdle idleState = new EnemyStateIdle();
    public EnemyStateChasing chasingState = new EnemyStateChasing();
    public EnemyStateAttacking attackingState = new EnemyStateAttacking();

    [SerializeField]
    public Transform target;

    private void Start()
    {
        TransitionToState(idleState);
    }

    private void Update()
    {
        currentState.Update(this);
    }

    public void TransitionToState(EnemyStateBase state)
    {
        currentState = state;
        currentState.EnterState(this);
    }
}
