﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateChasing : EnemyStateBase
{
    private float moveSpeed = 3;
    private float attackRange = 2f;
    public override void EnterState(EnemyFSM enemy)
    {
        Debug.Log("Enter state " + this);
        enemy.GetComponent<SpriteRenderer>().color = Color.yellow;
    }

    public override void Update(EnemyFSM enemy)
    {
        Debug.Log("Update state " + this);
        float distance = Vector2.Distance(enemy.transform.position, enemy.target.position);
        enemy.transform.position += (enemy.target.position - enemy.transform.position).normalized * Time.deltaTime * moveSpeed;
        if (distance < attackRange)
        {
            enemy.TransitionToState(enemy.attackingState);
        }
    }

}
