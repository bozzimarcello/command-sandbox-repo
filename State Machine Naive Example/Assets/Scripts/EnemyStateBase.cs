﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyStateBase
{

    public abstract void EnterState(EnemyFSM enemy);

    public abstract void Update(EnemyFSM enemy);

}
