﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateIdle : EnemyStateBase
{
    private float chaseRange = 5f;

    public override void EnterState(EnemyFSM enemy)
    {
        Debug.Log("Enter state " + this);
        enemy.GetComponent<SpriteRenderer>().color = Color.white;
    }

    public override void Update(EnemyFSM enemy)
    {
        Debug.Log("Update state " + this);
        float distance = Vector2.Distance(enemy.transform.position, enemy.target.position);
        if (distance < chaseRange)
        {
            enemy.TransitionToState(enemy.chasingState);
        }
    }
}
