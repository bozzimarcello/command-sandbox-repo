﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateAttacking : EnemyStateBase
{
    private float moveSpeed = 3;

    public override void EnterState(EnemyFSM enemy)
    {
        Debug.Log("Enter state " + this);
        enemy.GetComponent<SpriteRenderer>().color = Color.red;
    }

    public override void Update(EnemyFSM enemy)
    {
        Debug.Log("Update state " + this);
        enemy.transform.position += (enemy.target.position - enemy.transform.position).normalized * Time.deltaTime * moveSpeed;
        enemy.TransitionToState(enemy.idleState);
    }

}
