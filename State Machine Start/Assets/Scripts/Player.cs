﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    [Header("Player")]
    [SerializeField] float moveSpeed = 10f;
    [SerializeField] float xPadding = 2f;
    [SerializeField] float yPadding = 1f;
    [SerializeField] Vector3 pistolOffset;


    [Header("Projectile")]
    [SerializeField] GameObject projectilePrefab;
    [SerializeField] float projectileSpeed = 800f;
    [SerializeField] float projectileFiringPériod = 0.05f;

    float xMin;
    float xMax;
    float yMin;
    float yMax;

    //    Coroutine fireCoroutine;

    void Start()
    {
        SetupMoveBoundaries();
    }

    private void SetupMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + xPadding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - xPadding;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + yPadding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - yPadding;
    }

    void Update()
    {
        Move();
        Fire();
    }

    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax);
        var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);

        transform.position = new Vector2(newXPos, newYPos);
    }

    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Vector2 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 direction = (mouseWorldPosition - (Vector2)transform.position).normalized;

            GameObject laser = Instantiate(projectilePrefab, transform.position + pistolOffset, Quaternion.identity) as GameObject;
            laser.GetComponent<Rigidbody2D>().velocity = direction * projectileSpeed * Time.deltaTime;
        }
    }

    /*
    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {

            fireCoroutine = StartCoroutine(FireContinously());
        }
        if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(fireCoroutine);
        }
    }

    IEnumerator FireContinously()
    {
        while (true)
        {
            Vector2 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 direction = (mouseWorldPosition - (Vector2)transform.position).normalized;

            GameObject laser = Instantiate(projectilePrefab, transform.position + pistolOffset, Quaternion.identity) as GameObject;
            laser.GetComponent<Rigidbody2D>().velocity = direction * projectileSpeed * Time.deltaTime;

            yield return new WaitForSeconds(projectileFiringPériod);
        }
    }
    */

}
