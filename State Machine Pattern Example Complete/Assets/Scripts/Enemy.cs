﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private Transform player;

    private float chaseRange = 5f;
    private float attackRange = 2f;
    private float moveSpeed = 3f;

    private bool isChasing = false;
    private bool isAttacking = false;
    private bool isIdle = true;

    void Update()
    {
        float distance = Vector2.Distance(transform.position, player.position);
        Debug.Log("distance: " + distance);

        if (isIdle)
        {
            Debug.Log("is idle");
            GetComponent<SpriteRenderer>().color = Color.white;
            if (distance < chaseRange)
            {
                isChasing = true;
                isIdle = false;
            }
        }
        else if (isChasing)
        {
            Debug.Log("is chasing");
            transform.position += (player.position - transform.position).normalized * Time.deltaTime * moveSpeed;
            if (distance < attackRange)
            {
                isAttacking = true;
                isChasing = false;
            }
        } else if (isAttacking)
        {
            Debug.Log("is attacking");
            transform.position += (player.position - transform.position).normalized * Time.deltaTime * moveSpeed;
            GetComponent<SpriteRenderer>().color = Color.red;
            isAttacking = false;
            isIdle = true;
        }

    }
}
