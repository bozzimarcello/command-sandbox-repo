﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBaseState
{
    public abstract void EnterState(Enemy_FSM enemy);
    public abstract void Update(Enemy_FSM enemy);

}
