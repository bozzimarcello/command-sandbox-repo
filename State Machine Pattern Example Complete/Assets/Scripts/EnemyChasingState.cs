﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChasingState : EnemyBaseState
{
    private float moveSpeed = 3f;
    private float attackRange = 2f;
    public override void EnterState(Enemy_FSM enemy)
    {
        Debug.Log("Enter state " + this);
        enemy.GetComponent<SpriteRenderer>().color = Color.yellow;
    }
    public override void Update(Enemy_FSM enemy)
    {
        Debug.Log("Update state " + this);
        float distance = Vector2.Distance(enemy.transform.position, enemy.target.position);
        enemy.transform.position += (enemy.target.position - enemy.transform.position).normalized * Time.deltaTime * moveSpeed;
        if (distance < attackRange)
        {
            enemy.TransitionToState(enemy.attackingState);
        }
    }
}
