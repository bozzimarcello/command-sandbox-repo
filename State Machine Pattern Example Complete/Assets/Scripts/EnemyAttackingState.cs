﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackingState : EnemyBaseState
{
    private float moveSpeed = 3f;
    public override void EnterState(Enemy_FSM enemy)
    {
        Debug.Log("Enter state " + this);
        enemy.GetComponent<SpriteRenderer>().color = Color.red;
    }
    public override void Update(Enemy_FSM enemy)
    {
        Debug.Log("Update state " + this);
        enemy.transform.position += (enemy.target.position - enemy.transform.position).normalized * Time.deltaTime * moveSpeed;
        enemy.TransitionToState(enemy.idleState);
    }
}
