﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_FSM : MonoBehaviour
{
    public Transform target;

    private EnemyBaseState currentState;

    public EnemyIdleState idleState = new EnemyIdleState();
    public EnemyChasingState chasingState = new EnemyChasingState();
    public EnemyAttackingState attackingState = new EnemyAttackingState();

    void Start()
    {
        TransitionToState(idleState);
    }

    // Update is called once per frame
    void Update()
    {
        currentState.Update(this);
    }

    public void TransitionToState(EnemyBaseState state)
    {
        currentState = state;
        currentState.EnterState(this);
    }

}
