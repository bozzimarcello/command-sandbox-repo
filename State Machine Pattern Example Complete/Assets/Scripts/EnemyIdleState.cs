﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdleState : EnemyBaseState
{
    private float chaseRange = 5f;
    public override void EnterState(Enemy_FSM enemy)
    {
        Debug.Log("Enter state " + this);
        enemy.GetComponent<SpriteRenderer>().color = Color.white;
    }
    public override void Update(Enemy_FSM enemy)
    {
        Debug.Log("Update state " + this);
        float distance = Vector2.Distance(enemy.transform.position, enemy.target.position);
        if (distance < chaseRange)
        {
            enemy.TransitionToState(enemy.chasingState);
        }
    }
}
